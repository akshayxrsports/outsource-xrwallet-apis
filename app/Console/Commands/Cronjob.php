<?php

namespace App\Console\Commands;

use App\DripEmailer;
use App\Http\Controllers\WalletController;
use Illuminate\Console\Command;

class Cronjob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:cron-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto cron for marketplace';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \App\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
        $controller = new WalletController();
        $controller->cronJob();
    }
}
