<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestAmazonSes;
use DB;
use App\Models\User;
use Validator; 
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Str;
class WalletControllerNear extends Controller { 
    
    /* create wallet API */
    public function createWalletNear(Request $request){
        try{
            
            $postData = ["data" => $request->data['walletAesFormat']];
            $jsonData = json_encode($request->data);
            //echo "<pre>adadad"; print_r($jsonData); die();
            if(isset($request->data['hmacEncryption']) && $request->data['hmacEncryption'] != '' && isset($request->data['walletAesFormat']) && $request->data['walletAesFormat'] !=''){
                /* ------ create user wallet account ----------  */
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI_NEAR').'/create_user',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    // CURLOPT_POSTFIELDS => http_build_query($postData),
                    CURLOPT_POSTFIELDS => $jsonData, // Send JSON data
                    // CURLOPT_HTTPHEADER => array(
                    //     'hmac:'. $request->data['hmacEncryption'],
                    //     'Content-Type: application/x-www-form-urlencoded'
                    // ),
                    CURLOPT_HTTPHEADER => array(
                        'hmac:' . $request->data['hmacEncryption'],
                        'Content-Type: application/json' // Set the content type to JSON
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                //echo "<pre>adadad"; print_r($response); die();

                /* get user Waller Address and Data */
                if(isset($response) && $response !=null && $response !=null){
                    $postDataUserData = ["encData" => json_decode($response)->user] ;
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => http_build_query($postDataUserData),
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/x-www-form-urlencoded'
                        ),
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                    return $response;
                    
                }else{
                    return ['status'=> 2, 'data' => 'something went wrong'];
                }
            }
        }catch(\Exception $e){
            dd($e);
        }
    }
    /*-- end here --*/

    /*--- Apply wallet authentication for user's wallet ---*/
    public function createWalletAuthNear($data){
        try{
            $authType = $data['multifactor_authentication'] === 'otp_auth' ? 'otp' : 'google_auth';
            $userDetail = DB::table('zag_wallet_users')->whereWalletUserId($data['creatorid'])->whereZapWalletAddress($data['zap_wallet_address'])->first();
            $userDetailData = DB::table('users')->whereUserId($data['creatorid'])->first();
            if($userDetail){
                if($data['multifactor_authentication'] === 'google_auth')
                    $userDataUpdate = DB::table('zag_wallet_users')->whereWalletUserId($userDetail->wallet_user_id)->whereZapWalletAddress($data['zap_wallet_address'])->update([ 'zap_wallet_auth' => $authType, 'zag_wallet_gauth_key' => $data['gAuthKey'] ]);
                else
                    if($data['phone'] === $userDetailData->phone)
                        $userDataUpdate = DB::table('zag_wallet_users')->whereWalletUserId($userDetail->wallet_user_id)->whereZapWalletAddress($data['zap_wallet_address'])->update(['zap_wallet_auth' => $authType]);
                    else
                        $userDataUpdate = DB::table('zag_wallet_users')->whereWalletUserId($userDetail->wallet_user_id)->whereZapWalletAddress($data['zap_wallet_address'])->update([ 'zap_wallet_auth' => $authType, 'zag_wallet_phone' => $data['phone'] ]);
                    
            }
            $userDetailData = DB::table('zag_wallet_users')->whereWalletUserId($data['creatorid'])->whereZapWalletAddress($data['zap_wallet_address'])->first();
            $authType === 'otp_auth' ? $authMessage = 'One time password successfully applied to your wallet' : $authMessage = 'Google authentication successfully applied to your wallet';  
            isset($userDataUpdate) ? $array2 = [ 'response' => '1','message' => $authMessage , 'user_detail' => $userDetailData ] : $array2 = [ 'response' => '0','message' => 'Something Went Wrong !' ];
            return ['data' => $array2];

        }catch(\Exception $e){
            dd($e);
        }
    }
    /* -- end here --*/

    /* get wallet information  */
    public function getWalletInformationNear (Request $request){

        // echo "<pre>"; print_r($request->data ); die();
        try{
            $walletAesFormat = $request->data['AESEncryption'];
            $walletHmacFormat  = $request->data['HMACEncryption'];
            if(isset($walletAesFormat) && $walletAesFormat !='' && $walletAesFormat !='undefined' && $walletAesFormat !=null ){
                // return $request;
                $curl = curl_init();
                $walletAesFormat = [ 'data' => $walletAesFormat ];
                $walletAesFormat = json_encode($walletAesFormat);
                curl_setopt_array($curl, array(
                    CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI_NEAR').'/walletInformation',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    // CURLOPT_POSTFIELDS => http_build_query($walletAesFormat),
                    CURLOPT_POSTFIELDS =>$walletAesFormat,
                    // CURLOPT_HTTPHEADER => array(
                    //     'hmac:'.$walletHmacFormat,
                    //     'Content-Type: application/x-www-form-urlencoded'
                    // ),
                    CURLOPT_HTTPHEADER => array(
                        'hmac:'.$walletHmacFormat,
                        'Content-Type: application/json' // Set the content type to JSON
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
               //echo "<pre> dsfdfsdfsd"; print_r($response ); die();
 
                if($response != false){
                    /*--- decrypt wallet info ---*/
                    if(json_decode($response) !=null && json_decode($response)->status == '1') { 
                        $curl = curl_init();
                        $postDataUserDatas = [ 'encData' => json_decode($response)->data ];

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($postDataUserDatas),
                            CURLOPT_HTTPHEADER => array(
                                'Content-Type: application/x-www-form-urlencoded'
                            ),
                        ));
                        $response = curl_exec($curl);
                        curl_close($curl);
                        // $walletUserData = DB::table('zag_wallet_users')->whereZapWalletAddress(json_decode($response)->xp_walletAddress)->first();
                        // return [ 'data' => json_decode($response) ];
                        return $response;
                    }else if(json_decode($response)->status == '0' && isset(json_decode($response)->errors->xp_walletAddress)){
                        // $array2 = ['status' => json_decode($response)->status , 'message' => json_decode($response)->errors->xp_walletAddress];
                        // return response()->json(['data' => $array2]);
                        return [ 'data' =>  json_decode($response)->errors->xp_walletAddress ];
                    }else if(json_decode($response)->status == '0' && isset(json_decode($response)->errors->xp_password)){
                        // $array2 = ['status' => json_decode($response)->status , 'message' => json_decode($response)->errors->xp_walletAddress];
                        // return response()->json(['data' => $array2]);
                        return [ 'data' =>  json_decode($response)->errors->xp_password ];
                    }else{
                        return [ 'data' => 'something went wrong g' ];
                    }
                }else{
                    return [ 'data' => 'server error 1' ];
                }
                
            }
        }catch(\Exception $e){
            dd($e);
        }
    }
    /* -- end here --*/

    public function getWalletInformationLoginNear(Request $request ){
        try{
            $walletAesFormat = $request->data['AESEncryption'];
            $walletHmacFormat = $request->data['HMACEncryption'];
            if(isset($walletAesFormat) && $walletAesFormat !='' && $walletAesFormat !='undefined' && $walletAesFormat !=null ){
                $curl = curl_init();
                $walletAesFormat = [ 'data' => $walletAesFormat ];
                $walletAesFormat = json_encode($walletAesFormat);
                curl_setopt_array($curl, array(
                    CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI_NEAR').'/walletInformation',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    // CURLOPT_POSTFIELDS => http_build_query($walletAesFormat),
                    CURLOPT_POSTFIELDS =>$walletAesFormat,
                    // CURLOPT_HTTPHEADER => array(
                    //     'hmac:'.$walletHmacFormat,
                    //     'Content-Type: application/x-www-form-urlencoded'
                    // ),
                    CURLOPT_HTTPHEADER => array(
                        'hmac:'.$walletHmacFormat,
                        'Content-Type: application/json' // Set the content type to JSON
                    ),
                ));
                $response = curl_exec($curl);
               // echo "<pre>"; print_r($response); die();
                curl_close($curl);
                if($response != false){
                    /*--- decrypt wallet info ---*/
                    if(json_decode($response)->status == '1') { 
                        $curl = curl_init();
                        $postDataUserDatas = [ 'encData' => json_decode($response)->data ];
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($postDataUserDatas),
                            CURLOPT_HTTPHEADER => array(
                                'Content-Type: application/x-www-form-urlencoded'
                            ),
                        ));
                        $response1 = curl_exec($curl);
                        curl_close($curl);
                        // echo "<pre>"; print_r($response1); die();
                        return $response1;
                    }else if(json_decode($response)->status == '0'){
                        return $response;
                    }else{
                        return [ 'data' => 'something went wrong' ];
                    }
                }else{
                    return [ 'data' => 'server error' ];
                }
                
            }
        }catch(\Exception $e){
            dd($e);
        }
    }

    /* --- Reset Wallet API function --- */
    public function resetWalletPassword(Request $request){
            if(isset($request->data['HMACEncryption']) && $request->data['HMACEncryption'] != '' && isset($request->data['AESEncryption']) && $request->data['AESEncryption'] !=''){
                $curl = curl_init();
                $walletAesFormat = [ 'data' => $request->data['AESEncryption'] ];
                curl_setopt_array($curl, array(
                    CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/changePassword',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>  http_build_query($walletAesFormat),
                    CURLOPT_HTTPHEADER => array(
                        'hmac: '.$request->data['HMACEncryption'],
                        'Content-Type: application/x-www-form-urlencoded'
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                return $response;
                
            }else{
                return [ 'status' => 2 , 'data' => 'something went wrong' ];
            }
    }
     /* -- end here --*/

        public function updateWalletAuth(Request $request){
            try{
                $authType = $request->multifactor_authentication === 'otp_auth' ? 'otp' : 'google_auth';
                $userDetail = DB::table('zag_wallet_users')->whereWalletUserId($request->creatorid)->whereZapWalletAddress($request->zap_wallet_address)->first();
                $userDetailData = DB::table('users')->whereUserId($request->creatorid)->first();
                if($userDetail){
                    if($request->multifactor_authentication === 'google_auth')
                        $userDataUpdate = DB::table('zag_wallet_users')->whereWalletUserId($request->creatorid)->whereZapWalletAddress($request->zap_wallet_address)->update([ 'zap_wallet_auth' => $authType, 'zag_wallet_gauth_key' => $request->gAuthKey ,'zag_wallet_phone' => NULL]);
                    else
                        if($request->phone === $userDetailData->phone)
                            $userDataUpdate = DB::table('zag_wallet_users')->whereWalletUserId($request->creatorid)->whereZapWalletAddress($request->zap_wallet_address)->update(['zap_wallet_auth' => $authType, 'zag_wallet_gauth_key' => NULL]);
                        else
                            $userDataUpdate = DB::table('zag_wallet_users')->whereWalletUserId($request->creatorid)->whereZapWalletAddress($request->zap_wallet_address)->update([ 'zap_wallet_auth' => $authType, 'zag_wallet_phone' => $request->phone ,'zag_wallet_gauth_key' => NULL]);
                        
                }
                $userDetailData = DB::table('zag_wallet_users')->whereWalletUserId($request->creatorid)->whereZapWalletAddress($request->zap_wallet_address)->first();
                $authType === 'otp_auth' ? $authMessage = 'One time password successfully applied to your wallet' : $authMessage = 'Google authentication successfully applied to your wallet';  
                isset($userDataUpdate) ? $array2 = [ 'response' => '1','message' => $authMessage , 'user_detail' => $userDetailData ] : $array2 = [ 'response' => '0','message' => 'Something Went Wrong !' ];
                return response()->json(['data' => $array2]);
            }catch(\Exception $e){
                dd($e);
            }
        }


        public function getWalletUserDetails($data){
            if(isset($data['walletAddress'])){
                $userWalletInfo = DB::table('zag_wallet_users')->whereWalletUserId($data['userId'])->whereZapWalletAddress($data['walletAddress'])->first();
                return ['data' => $userWalletInfo]; 
            }else{
                return ['data' => []];
            }
        }

        public function getUserAllWallet($data){
            if(isset($data['userId'])){
                $userWallets = DB::table('zag_wallet_users')->whereWalletUserId($data['userId'])->get();
                return ['data' => $userWallets]; 
            }else{
                return ['data' => []];
            }
        }
        
        /* --- Credit tokens API --  */
        public function creditTokens(Request $request){
            try{            
                
                $walletAesFormat = $request->data['walletAesFormat'];
                $walletHmacFormat  = $request->data['hmacEncryption'];

                $walletAesFormat = [ 'data' => $walletAesFormat ];
                $curl = curl_init();
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/trade/sendTreasuryToken',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => http_build_query($walletAesFormat),
                        CURLOPT_HTTPHEADER => array(
                            'hmac: '. $walletHmacFormat,
                            'Content-Type: application/x-www-form-urlencoded'
                        ),
                    ));

                    $response = curl_exec($curl);
                   // echo "<pre>"; print_r($response); die();
                    curl_close($curl);
                    if($response != false){
                        /*--- decrypt wallet info ---*/
                        if(json_decode($response) !=null && json_decode($response)->status == '1') { 
                            $curl = curl_init();
                            $postDataUserDatas = [ 'encData' => json_decode($response)->tx ];
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => '',
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => 'POST',
                                CURLOPT_POSTFIELDS => http_build_query($postDataUserDatas),
                                CURLOPT_HTTPHEADER => array(
                                    'Content-Type: application/x-www-form-urlencoded'
                                ),
                            ));
                            $response1 = curl_exec($curl);
                            curl_close($curl);
                            return response()->json([ 'data' => json_decode($response1) ]);
                        }else{
                            return response()->json([ 'data' => 'something went wrong' ]);
                        }
                    }else{
                        return response()->json([ 'data' => 'server error' ]);
                    }
            }catch(\Exception $e){
                dd($e);
            }
        }
    /* -- send balance,xp,matic,eth as well --  */
    public function sendXpsNear(Request $request){
        // return [$request->data['hmacEncryption'], $request->data['walletAesFormat']];
        //echo "<pre>"; print_r($request->data['walletAesFormat']); die();
        try {
            $curl = curl_init();
            $postData = [ 'data' => $request->data['walletAesFormat']];
            $postData = json_encode($postData);

            if($request->data['token_type'] === 'near'){
                $end_point = '/send_near';
            }
            if($request->data['token_type'] === 'xp'){
                $end_point = '/trade/sendXp';
            }
            if($request->data['token_type'] === 'actv'){
                $end_point = '/trade/sendXp';
            }
            if($request->data['token_type'] === 'eth'){
                $end_point = '/trade/sendEth';
            }
            if($request->data['token_type'] === 'matic'){
                $end_point = '/trade/sendMatic';
            }
            curl_setopt_array($curl, array(
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI_NEAR').$end_point,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                // CURLOPT_POSTFIELDS => http_build_query($postData),
                CURLOPT_POSTFIELDS =>$postData,
                // CURLOPT_HTTPHEADER => array(
                    //     'hmac:'.$request->data['hmacEncryption'],
                    //     'Content-Type: application/x-www-form-urlencoded'
                    // ),
                    CURLOPT_HTTPHEADER => array(
                        'hmac:'.$request->data['hmacEncryption'],
                        'Content-Type: application/json' // Set the content type to JSON
                    ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
           // echo "<pre>"; print_r($response); die(); 
            if($response != false){
                $sendXPStatus = json_decode($response)->status;
                if($sendXPStatus != '0'){
                    if(json_decode($response)->tx != '') {
                        $curl = curl_init();
                        $postDataUserDatas = [ 'encData' => json_decode($response)->tx ];
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($postDataUserDatas),
                            CURLOPT_HTTPHEADER => array(
                                'Content-Type: application/x-www-form-urlencoded'
                            ),
                        ));
                        $responseDec = curl_exec($curl);
                        curl_close($curl);
                        // return $responseDec;
                        $array2 = ['response' => '1', 'wallet' => json_decode($responseDec), 'message' => 'Successfully sent tokens' ];
                        // $this->postDataToUsersWalletActivity($request->data);
                        return ['data' => $array2];
                    }
                }else{
                    $array2 = ['response' => '0', 'message' => 'Insufficient funds for intrinsic transaction' ];
                    return ['data' => $array2];
                } 
            }else{
                $array2 = ['response' => '2', 'message' => 'something went wrong' ];
                return ['data' => $array2];
            }
             
        } catch (\Exception $th) {
            dd($th);
        }

    }


    public function postDataToUsersWalletActivity(Request $request){
        if(isset($request)){
            $curl = curl_init();
            $postDataUserDatasArray = [ 'encData' => $request->walletAesFormat ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($postDataUserDatasArray),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
            ));
            $responseDecJson = curl_exec($curl);
            curl_close($curl);
            $dataToBePostJson = json_decode($responseDecJson);

            $activityData =  array([
                'tracking_id' => Carbon::now()->parse()->format('Ydmhi'),
                'activity_type' => 'Send',
                'transaction_from' => 'Wallet',
                'transaction_by' => 'Wallet',
                'token_type' => $request->token_type,
                'token_value' => $dataToBePostJson->xp_tokens,
                'sender_wallet_address' => $dataToBePostJson->xp_senderAddress,
                'receiver_wallet_address' => $dataToBePostJson->xp_receiverAddress,
            ]);
            $xapWalletactivity = DB::table('zag_wallet_activities')->insert($activityData);
        }
    }

    /* ------ Wallet one time password authentication ------ */
    public function oneTimePasswordForWallet(Request $request) {
        if(isset($request->creatorid) && $request->creatorid !='' ){
            $userData = DB::table('users')->whereUserId($request->creatorid)->first();
            if(isset($userData)){
                if(isset($request->phone) && $request->phone !='' ){
                    $otpForWallet = rand(111111,999999);
                    $phone = $request->phone;
                    $clientname = getenv('NAMECLIENT');
                    try{
                        $account_sid = getenv("TWILIO_SID");
                        $auth_token = getenv("TWILIO_AUTH_TOKEN");
                        $twilio_number = getenv("TWILIO_NUMBER");
                        $message = "Hello ".$userData->firstname.", " .$otpForWallet. " is your Phone Verification Code for Login Wallet with ".$clientname." This OTP is valid for only 60 seconds. Please don't share with anyone.";
                        $correctmessage = str_replace("%20"," ",$message);
                        $client = new Client($account_sid, $auth_token);
                        $twilioMessage = $client->messages->create($phone, ['from' => $twilio_number, 'body' => $correctmessage] );
                            return response()->json([
                                'ResponseCode' => '1',
                                'ResponseText' => 'Verified.',
                                'type_forward' => base64_encode(base64_encode($otpForWallet))
                            ]);
                    } catch (\Exception $e){
                            dd($e);
                    }
                }
            }
        }
    }

    public function getWalletActivities(Request $request){
        try{
            $getWalletActivitiesSend = DB::table('zag_wallet_activities')->whereSenderWalletAddress($request->walletAddress)
            ->leftjoin('zag_wallet_users', 'zag_wallet_users.zap_wallet_address' ,'=', 'zag_wallet_activities.receiver_wallet_address')
            ->leftjoin('users', 'users.user_id' ,'=', 'zag_wallet_users.wallet_user_id')
            ->select( 'zag_wallet_activities.id','zag_wallet_activities.activity_type','zag_wallet_activities.transaction_from','zag_wallet_activities.transaction_by','zag_wallet_activities.token_type'
                    ,'zag_wallet_activities.token_value', 'zag_wallet_activities.sender_wallet_address','zag_wallet_activities.receiver_wallet_address','zag_wallet_activities.bank_account_number'
                    ,'zag_wallet_activities.created_at','users.firstname', 'users.user_id'
            )->orderBy('created_at','DESC')->get()->toArray();
            
            $getWalletActivitiesReceive = DB::table('zag_wallet_activities')->whereReceiverWalletAddress($request->walletAddress)
            ->leftjoin('zag_wallet_users', 'zag_wallet_users.zap_wallet_address' ,'=', 'zag_wallet_activities.sender_wallet_address')
            ->leftjoin('users', 'users.user_id' ,'=', 'zag_wallet_users.wallet_user_id')
            ->select( 'zag_wallet_activities.id','zag_wallet_activities.activity_type','zag_wallet_activities.transaction_from','zag_wallet_activities.transaction_by','zag_wallet_activities.token_type'
                    ,'zag_wallet_activities.token_value', 'zag_wallet_activities.sender_wallet_address','zag_wallet_activities.receiver_wallet_address','zag_wallet_activities.bank_account_number'
                    ,'zag_wallet_activities.created_at','users.firstname', 'users.user_id'
            )->orderBy('created_at','DESC')->get()->toArray();
            $array_merge = array_merge($getWalletActivitiesReceive, $getWalletActivitiesSend);
            return response()->json([ 'response' => '1', 'data' => $array_merge ]);
        }catch(\Exception $e){
            dd($e);
        }
    } 

    public function getLimitedWalletActivities(Request $request){
        try{
            $getWalletActivitiesSend = DB::table('zag_wallet_activities')->whereSenderWalletAddress($request->walletAddress)
                ->leftjoin('zag_wallet_users', 'zag_wallet_users.zap_wallet_address' ,'=', 'zag_wallet_activities.receiver_wallet_address')
                ->leftjoin('users', 'users.user_id' ,'=', 'zag_wallet_users.wallet_user_id')
                ->select( 'zag_wallet_activities.id','zag_wallet_activities.activity_type','zag_wallet_activities.transaction_from','zag_wallet_activities.transaction_by','zag_wallet_activities.token_type'
                        ,'zag_wallet_activities.token_value', 'zag_wallet_activities.sender_wallet_address','zag_wallet_activities.receiver_wallet_address','zag_wallet_activities.bank_account_number'
                        ,'zag_wallet_activities.created_at','users.firstname', 'users.user_id' ); 
            $getWalletActivitiesReceive = DB::table('zag_wallet_activities')->whereReceiverWalletAddress($request->walletAddress)
                ->leftjoin('zag_wallet_users', 'zag_wallet_users.zap_wallet_address' ,'=', 'zag_wallet_activities.sender_wallet_address')
                ->leftjoin('users', 'users.user_id' ,'=', 'zag_wallet_users.wallet_user_id')
                ->select( 'zag_wallet_activities.id','zag_wallet_activities.activity_type','zag_wallet_activities.transaction_from','zag_wallet_activities.transaction_by','zag_wallet_activities.token_type'
                        ,'zag_wallet_activities.token_value', 'zag_wallet_activities.sender_wallet_address','zag_wallet_activities.receiver_wallet_address','zag_wallet_activities.bank_account_number'
                        ,'zag_wallet_activities.created_at','users.firstname', 'users.user_id' )
            ->union($getWalletActivitiesSend)
            ->orderBy('created_at','DESC')->limit(3)->get()->toArray();
            return response()->json([ 'response' => '1', 'data' => $getWalletActivitiesReceive ]);
        }catch(\Exception $e){
            dd($e);
        }
    } 




    public function whitelistApi(Request $request){

        $walletAddressPost =  $request->walletAddress;
        //echo "<pre>"; print_r( $walletAddressPost); die();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/addWhitelist',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => http_build_query($walletAddressPost),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
                
    }


    public function decrypt(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $curl = curl_init();
        $postDataUserDatasArray = [ 'encData' => $request->encdata ];
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/decrypt',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($postDataUserDatasArray),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));
        $responseDecJson = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $responseDecJson;
    }  


    public function mintnft(Request $request){

        // echo "<pre>"; print_r($request->file);
        //  echo "<pre>"; print_r($_FILES);  die();

         $postFieldsData = $request->except(['nftMedia']);
    //  echo "<pre> qwqw"; print_r($postFieldsData); die();
        // $file = new \CURLFILE($_FILES['nftMedia']['tmp_name'],$_FILES['nftMedia']['type'],$_FILES['nftMedia']['name']) ;
        $postFieldsData['nftMedia'] = $request->nftMedia;
        //   echo "<pre> qwqw"; print_r($postFieldsData); die();
        $curl = curl_init();
        // $postDataUserDatasArray = [ 'encData' => $request->encdata ];
        curl_setopt_array($curl, array(
          CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/mint',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => $postFieldsData,
        //   CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/x-www-form-urlencoded'
        //   ),
        ));

        
        $response = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }

    public function getMintData(Request $request){
        // echo "<pre>"; print_r($request->pagination); die();
        $curl = curl_init();
              curl_setopt_array($curl, array(
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getMintData?'.$request->pagination,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => "",
              ));
        $response = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }


    
    public function getPlaceOrderDataById(Request $request){
        //  echo "<pre>"; print_r($request->placeOrderId); die("okkkk");
        $placeOrderId = array('placeOrderId' => $request->placeOrderId);
        $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getPlaceOrderDataById',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => http_build_query($placeOrderId),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }
    
    public function getMintDataById(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $mintId = array('mintId' => $request->mintId);
               $curl = curl_init();
              curl_setopt_array($curl, array(
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getMintDataById',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($mintId),
                CURLOPT_HTTPHEADER => array(
                  'Content-Type: application/x-www-form-urlencoded'
                ),
              ));
              $responseMint = curl_exec($curl);
        //echo "<pre>"; print_r( $response); die();
        return $responseMint;
    }

    public function placeOrder(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $postFields = $request->postFields;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/placeOrder',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => http_build_query($postFields),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        $response = curl_exec($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }

    
    public function setApproval(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $postData = $request->postData;
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/setApproval',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($postData),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
            ));
            $response = curl_exec($curl);
            //dd($response);
            curl_close($curl);
            return $response;
    }
    
    public function getPlaceOrderData(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $curl = curl_init();
            curl_setopt_array($curl, array(
            //   CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getPlaceOrderData',
              CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getPlaceOrderData?'.$request->pagination,
              // .$pagination,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => "",
            ));
            $response = curl_exec($curl);
            //echo "<pre>"; print_r($response); die();
            $http = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if ($http == 200) {
                return $response;
            } else {
                return 'There was a problem fetching data...';
            }
    }

    public function cronJob(Request $request){
        // echo "<pre>"; print_r($request->encdata);
         $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/cronJob',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
          ));
          $response = curl_exec($curl);
          curl_close($curl);
          return $response;

    }
   
    
    public function getApprovalDataByWalletAddress(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $postData = $request->postData;
        $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getApprovalDataByWalletAddress',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => http_build_query($postData),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
    }

    public function buynft(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $postFields = $request->postFields;
        $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/buy',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($postFields),
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));
        $response = curl_exec($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }
    
    public function approveWalletForBuyNft(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $postFields = $request->postFields;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/approve',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => http_build_query($postFields),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }


    public function getbuyNftData(Request $request){
        // echo "<pre>"; print_r($request->encdata);
        $postFields = $request->postFields;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getbuyNftData',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => http_build_query($postFields),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }

    
    public function deleteData(Request $request){
        $postFields = $request->postFields;
        $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/deleteData',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($postFields),
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));
          $response = curl_exec($curl);
          curl_close($curl);
    
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }

    public function getgasPrice(Request $request){
        $curl = curl_init();
              curl_setopt_array($curl, array(
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/trade/gasPrice',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => "",
              ));
          $response = curl_exec($curl);
          curl_close($curl);
    
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }

    
    public function getRoyalty(Request $request){
        $postFields = $request->postFields;
        $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/admin/getRoyalty',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => http_build_query($postFields),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
    
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }


    
    public function blockchainStatus(Request $request){
        $postFields = $request->postData;
//echo "<pre>"; print_r( $postFields); die();
        $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI_TRANSACTION').'/blockchainStatus/?'.http_build_query(['tx' => $postFields]),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
          ));
          $response = curl_exec($curl);
          curl_close($curl);
    
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }
    
    public function abledisbleNft(Request $request){
        $postFields = $request->postFields;
       
//  echo "<pre>"; print_r($postFields); die();
        if($postFields['status'] == "1"){
            // to make nft disable
            $url = config('global.WALLET_API_BASE_PATH_URI').'/admin/disable';
              $postFieldss = [
                'disableNfts' => $postFields['Nfts_id'], 
                'disableWallets' => $postFields['Wallets'],
                'walletAddress' => $postFields['walletAddress'],
                'password' =>  $postFields['password']
              ];
        }else{
        // to make nft enable
            $url = config('global.WALLET_API_BASE_PATH_URI').'/admin/enable';
            $postFieldss = [
            'enableNfts' => $postFields['Nfts_id'], 
            'enableWallets' => $postFields['Wallets'],
            'walletAddress' => $postFields['walletAddress'],
            'password' =>  $postFields['password']
            ];
        }
        $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($postFieldss),
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));
          $response = curl_exec($curl);
          curl_close($curl);
    
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }


    public function uploadFilenft(Request $request){
        $postFieldsData = $request;
    //  echo "<pre> qwqw"; print_r($_FILES); //die();
        $file = new \CURLFILE($_FILES['file']['tmp_name'],$_FILES['file']['type'],$_FILES['file']['name']) ;
        $postFieldsDataa = [
            'name' => $request['name'] ?? 'null',
            'description' => $request['description'] ?? 'null',
            'walletAddress' => $request['walletAddress'],
            'file' => $file,
          ]; 
        try{
            $curl = curl_init();
            curl_setopt_array($curl, array(
               CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/uploadFile',
              //CURLOPT_URL =>config('services.WALLET_REDIRECT_URI').'/uploadFilenft', // another domain url
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => $postFieldsDataa,
            ));
            $response = curl_exec($curl);
           // echo "<pre> AAAAAA"; print_r($response);  die();
               curl_close($curl);
            // if (curl_errno($curl)) {
            //     $error_msg = curl_error($curl);
            //     echo "<pre> AAAAAA"; print_r($error_msg); 
            // }
            //   $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            //   curl_close($curl);

            //    echo "<pre> AAAAAA"; print_r($response); 
            //    echo "<pre>"; print_r($http_status); die();
        }
        catch(\Exception $e){
            dd($e);
        }
        return $response;
    }


    public function getPlaceOrderDataByWalletAddress(Request $request){
       
        $postFields = $request->postFields;

        // echo "<pre>"; print_r($postFields); die();
          $postFieldss = [ 
            'walletAddress' => $postFields['walletAddress']
          ];
        $curl = curl_init();
            curl_setopt_array($curl, array(
            //   CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getPlaceOrderData',
              CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getPlaceOrderDataByWalletAddress?'.$postFields['pagination'],
              // .$pagination,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($postFieldss),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
            ));
            $response = curl_exec($curl);
            // echo "<pre>"; print_r($response); die();
            // 
            
            if($response){

                // $data_placed_all = json_decode($response);
                $data_placed = json_decode($response);
                // echo "<pre>"; print_r($data_placed); die();
                foreach ($data_placed->data as $key => &$value) {

                    // echo "<pre>"; print_r($value->mintId); die();


                    $mintId = array('mintId' => $value->mintId);
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getMintDataById',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => http_build_query($mintId),
                            CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/x-www-form-urlencoded'
                            ),
                        ));
                        $responseMint = curl_exec($curl);
                        // echo "<pre>"; print_r(json_decode($responseMint)->data); die();
                        $value->mintId= json_decode($responseMint)->data;
                        // echo "<pre>"; print_r($value); die();
                        // die();
                }
                // $data_placed->data = json_encode($data_placed);
                // echo "<pre>"; print_r($data_placed); die();
                // echo "<pre>"; print_r(json_encode($data_placed)); die();


                $http = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                if ($http == 200) {
                    return json_encode($data_placed);
                } else {
                    return 'There was a problem fetching data...';
                }

            } 

            $http = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if ($http == 200) {
                return $response;
            } else {
                return 'There was a problem fetching data...';
            }
    }


    public function getMintDataByWalletAddress(Request $request){
        //$postFields = $request->postFields;
            $postFields = $request->postFields;

        // echo "<pre>"; print_r($postFields); die(); 
          $postFieldss = [ 
            'walletAddress' => $postFields['walletAddress']
          ];
        $curl = curl_init();
              curl_setopt_array($curl, array(
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getMintDataByWalletAddress?'.$postFields['pagination'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($postFieldss),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
            ));
        $response = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }


    public function getMintDataByAuthUser(Request $request){
        //$postFields = $request->postFields;
            $postFields = $request->postFields;

        // echo "<pre>"; print_r($postFields); die(); 
            $postFieldss = [
                'xp_walletAddress' => $postFields['walletAddress'],
                'xp_password' => $postFields['walletPassword']
            ];
              $curl = curl_init();
              curl_setopt_array($curl, array(
                // CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getMintDataByAuthUser?'.$postFields['pagination'],
                CURLOPT_URL => config('global.WALLET_API_BASE_PATH_URI').'/nft/getMintDataByAuthUser',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($postFieldss),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
            ));
        $response = curl_exec($curl);
        curl_close($curl);
        //echo "<pre>"; print_r( $response); die();
        return $response;
    }


    public function getApiResponse(Request $request){
        return 'Your API is successfully working on ' .env('APP_URL'). ' domain';
    }


}
