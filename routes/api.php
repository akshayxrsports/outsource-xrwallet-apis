<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user(); 
    
}); 
Route::get('/get-api-response', 'WalletController@getApiResponse');


// near network api requests

 
Route::post('create_wallet_near','WalletControllerNear@createWalletNear');
Route::post('create_wallet_auth_near','WalletControllerNear@createWalletAuthNear');
Route::post('get-wallet-information-near', 'WalletControllerNear@getWalletInformationNear');
Route::post('send_xps_near', 'WalletControllerNear@sendXpsNear');
Route::post('get-wallet-information-login-near', 'WalletControllerNear@getWalletInformationLoginNear'); 
Route::post('mint-nft-near', 'WalletController@mintnftnear');
Route::post('single-nft-near', 'WalletController@singlenftNear');
Route::post('allcommunitybadgescontract', 'WalletController@allcommunitybadgescontract');
Route::post('allcommunitybadges', 'WalletController@allcommunitybadges');

Route::post('getMintDataByWalletAddressNear', 'WalletController@getMintDataByWalletAddressNear');
Route::post('transferNFTToWalletAddressNear', 'WalletController@transferNFTToWalletAddressNear');
Route::post('getMyBadgesByWalletAddressNear', 'WalletController@getMyBadgesByWalletAddressNear');

// polygon

Route::post('wallet-setup', 'CommonController@walletSetup'); 

Route::post('create_wallet','WalletController@createWallet');
Route::post('create_wallet_auth','WalletController@createWalletAuth');
Route::post('reset-wallet-password', 'WalletController@resetWalletPassword');
Route::post('get-wallet-information', 'WalletController@getWalletInformation');
Route::post('get-wallet-information-login', 'WalletController@getWalletInformationLogin'); 

Route::post('verify-google-auth', 'WalletController@verifyGoogleAuth');
Route::post('send_xps', 'WalletController@sendXps');
Route::post('createaccesskeywallet', 'WalletController@createaccesskeywallet');
Route::post('get_badge_validation', 'WalletController@get_badge_validation'); 
Route::post('get_owner_badges', 'WalletController@get_owner_badges'); 
Route::post('save_badge', 'WalletController@save_badge'); 
Route::post('otp-for-wallet','WalletController@oneTimePasswordForWallet');
Route::post('update_wallet_auth','WalletController@updateWalletAuth');
Route::post('credit-tokens', 'WalletController@creditTokens');
Route::post('get-wallet-user-details', 'WalletController@getWalletUserDetails');
Route::post('get-user-all-wallet', 'WalletController@getUserAllWallet');
Route::post('get-wallet-activities', 'WalletController@getWalletActivities');
Route::post('get-limited-wallet-activities', 'WalletController@getLimitedWalletActivities');
// Route::any('get-wallet-information-login', 'WalletController@getAPI');



Route::post('addWhitelist', 'WalletController@whitelistApi');
Route::post('decrypt', 'WalletController@decrypt');
Route::post('encrypt', 'WalletController@encrypt');
Route::post('hashEncrypt', 'WalletController@hashEncrypt');

Route::post('decryptKMS', 'WalletController@decryptKMS');
Route::post('encryptKMS', 'WalletController@encryptKMS');

Route::post('mint-nft', 'WalletController@mintnft');
Route::post('getMintData', 'WalletController@getMintData');
Route::post('getPlaceOrderDataById', 'WalletController@getPlaceOrderDataById');
Route::post('getMintDataById', 'WalletController@getMintDataById');
Route::post('placeOrder', 'WalletController@placeOrder');
Route::post('setApproval', 'WalletController@setApproval');
Route::post('getPlaceOrderData', 'WalletController@getPlaceOrderData');
Route::post('cronJob', 'WalletController@cronJob');
Route::post('getApprovalDataByWalletAddress', 'WalletController@getApprovalDataByWalletAddress');
Route::post('buynft', 'WalletController@buynft');
Route::post('approveWalletForBuyNft', 'WalletController@approveWalletForBuyNft');
Route::post('getbuyNftData', 'WalletController@getbuyNftData');
Route::post('deleteData', 'WalletController@deleteData');

Route::post('getgasPrice', 'WalletController@getgasPrice');
Route::post('getRoyalty', 'WalletController@getRoyalty');
Route::post('blockchainStatus', 'WalletController@blockchainStatus');
Route::post('abledisbleNft', 'WalletController@abledisbleNft');

Route::post('uploadFilenft', 'WalletController@uploadFilenft');

Route::post('getPlaceOrderDataByWalletAddress', 'WalletController@getPlaceOrderDataByWalletAddress');
Route::post('getMintDataByWalletAddress', 'WalletController@getMintDataByWalletAddress');
Route::post('getMintDataByAuthUser', 'WalletController@getMintDataByAuthUser');



