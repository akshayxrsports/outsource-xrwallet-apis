<?php

return [
    // Stable domain to access wallet  
     'WALLET_API_BASE_PATH_URI' => 'https://web3.activelink.io/api',

     'WALLET_API_BASE_PATH_URI_TRANSACTION' => 'https://web3.activelink.io',  

    // prodcution domain URI to access wallet APIs
//    'WALLET_API_BASE_PATH_URI' => 'http://3.35.64.86:8080/api',

//     'WALLET_API_BASE_PATH_URI_TRANSACTION' => 'http://3.35.64.86',


//near network chain

// 'WALLET_API_BASE_PATH_URI_NEAR' => 'http://localhost:3001',
   'WALLET_API_BASE_PATH_URI_NEAR' => 'https://near.activelink.io',

];